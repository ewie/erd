BEGIN {
  FS = ";"
  print "digraph {"
  print "  node [shape=none, fontname=\"sans-serif\"]"
}
{
  ncols = split($2, cols, ",");

  printf("  \"%s\" [label=<<table border='0' cellborder='1' cellspacing='0'><tr><td bgcolor='black'><font color='white'>%s</font></td></tr><tr><td><table border='0' cellspacing='0'>", $1, $1);
  for (i = 1; i <= ncols; i++) {
    printf("<tr><td align='left'>%s</td></tr>", cols[i]);
  }
  print "</table></td></tr></table>>]";

  nrefs = split($3, refs, ",");

  for (i = 1; i <= nrefs; i++) {
    printf("  \"%s\" -> \"%s\"\n", $1, refs[i]);
  }
}
END {
  print "}"
}

erd
===

Generate basic entity-relationship diagrams from the metadata provided
by a relational database.


Usage
-----

erd does not fully automate the process of rendering database metadata
into entity-relationship diagrams.  The process involves manual labor
to prepare the database metadata such that erd can work with it.

The metadata is first translated to DOT [1] using awk with program
dot.awk.  This output is then processed by dot which in turn generates
the desired output, a graphical representation in our case.

The metadata represents one table per line containing the table name,
the names of its columns, and the names of referenced tables.

For example, the following DDL:

  create table author (
    id integer not null primary key,
    name varchar(200) not null
  );

  create table book (
    id integer not null primary key,
    author_id integer not null references author (id),
    title varchar(200) not null,
    isbn char(13) not null unique
  );

  create table member (
    id integer not null primary key,
    name varchar(200) not null,
    joined_on date not null
  );

  create table borrow (
    book_id integer not null references book (id),
    member_id integer not null references member (id),
    issued_on date not null,
    returned_on date,
    primary key (book_id, member_id, issued_on),
    check (issued_on <= returned_on)
  );

is expected to have the following metadata:

  author;id,name;
  book;id,author_id,title,isbn;author
  borrow;book_id,member_id,issued_on,returned_on;book,member
  member;id,name,joined_on;


Assuming file metadata.csv contains the metadata as described above, we
can render the entity-relationship diagram using the following command:

  $ awk -fdot.awk metadata.csv | dot -Tsvg -odiagram.svg


PostgreSQL
----------

For PostgreSQL, use postgres-metadata.sql to generate the necessary
metadata and paste it into metadata.csv.  Then follow the standard usage
described above.

Using psql with some database testdb, for example:

  $ psql testdb
  testdb=> \i postgres-metadata.sql

postgres-metadata.sql uses the public schema by default.  This can be
changed in postgres-metadata.sql:

  \set schema '''testschema'''


Oracle Database
---------------

For Oracle Database, use oracle-metadata.sql to generate the necessary
metadata and paste it into metadata.csv.  Then follow the standard usage
described above.


References
----------

[1] Visit https://www.graphviz.org/documentation/ for information on DOT
    and related tools.


License
-------

erd is released under the BSD 3-Clause License, see COPYING.

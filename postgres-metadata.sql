-- using public schema by default, adjust if necessary
\set schema '''public'''

with
col as (
  select
    t.tablename as table_name,
    string_agg(a.attname, ',' order by a.attnum) as column_names
  from pg_tables t
  join pg_class c on c.relname = t.tablename
  join pg_attribute a on a.attrelid = c.oid
  where a.attnum >= 1 -- restrict to ordinary columns
    and t.schemaname = :schema
  group by table_name
),
ref as (
  select
    cl.relname as table_name,
    string_agg(r_cl.relname, ',' order by r_cl.relname) as table_names
  from pg_constraint co
  join pg_class cl on cl.oid = co.conrelid
  join pg_namespace ns on ns.oid = cl.relnamespace
  join pg_class r_cl on r_cl.oid = co.confrelid
  join pg_namespace r_ns on r_ns.oid = r_cl.relnamespace
  where co.contype = 'f' -- restrict to foreign key constraints
    and ns.nspname = :schema
  group by table_name
)
select
  col.table_name || ';' || col.column_names || ';' || coalesce(ref.table_names, '') as metadata
from col
left join ref
       on ref.table_name = col.table_name;

with
ref as (
  select
    c.table_name,
    listagg(r.table_name, ',') within group (order by r.table_name) as table_names
  from user_constraints c
  join user_constraints r
    on r.constraint_name = c.r_constraint_name
  where c.constraint_type = 'R'
  group by c.table_name
),
col as (
  select
    table_name,
    listagg(column_name, ',') within group (order by column_id) as column_names
  from user_tab_columns
  group by table_name
)
select
  col.table_name || ';' || col.column_names || ';' || ref.table_names || '' as metadata
from col
left join ref
       on ref.table_name = col.table_name;

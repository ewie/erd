create table author (
  id integer not null primary key,
  name varchar(200) not null
);

create table book (
  id integer not null primary key,
  author_id integer not null references author (id),
  title varchar(200) not null,
  isbn char(13) not null unique
);

create table member (
  id integer not null primary key,
  name varchar(200) not null,
  joined_on date not null
);

create table borrow (
  book_id integer not null references book (id),
  member_id integer not null references member (id),
  issued_on date not null,
  returned_on date,
  primary key (book_id, member_id, issued_on),
  check (issued_on <= returned_on)
);
